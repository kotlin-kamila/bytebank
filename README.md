# Bytebank Kotlin Application

Projeto para estudo da linguagem Kotlin (JVM/Application), realizado juntamente com a plataforma Alura. A persona de
cliente idealizada para esta aplicação é o banco Bytebank.

## Criando aplicação

Os passos para criação de uma aplicação podem ser acessados no
site [kotlinlang](https://kotlinlang.org/docs/jvm-get-started.html#create-an-application).

## Variáveis

Ao criar uma variável é necessário inicializar o seu valor, ainda que o tipo não esteja explícito a variavel será
estaticamente tipada dado o valor inserido. As variáveis podem ser definidas como mutáveis (`var`) ou como
imutáveis (`val`).

É possível a utilização de *String template* utilizando "$" antes da variável entre aspas duplas.

Ao receber uma variável como parâmetro é necessário indicar o seu tipo.

Tipos de dados primitivos
na [documentação](https://kotlinlang.org/docs/tutorials/kotlin-for-py/primitive-data-types-and-their-limitations.html).

Variáveis globais podem ser alteradas por diversas classes, comportamento indesejado. No Kotlin os tipos primitivos
sempre são classes. Smart cast, possível, mas evitável:

```
valor as Funcionario
...
if (valor is Funcionario)
```

## [Null safety](https://kotlinlang.org/docs/null-safety.html#safe-calls)

Non-null assertion: (!!), boa prática é não utilizá-lo. Variável nullable: `var b: String? = "abc"`.<br>
Safe call operator: `println(b?.length)`, printa o valor caso não nulo, caso contrário retorna null

Elvis Operator (:?) se objeto for nulo retorna o valor após o operador `?:`.<br> 
Safe Cast `as?`, caso o casting não for
possível é retornado valor null.

## Object

De acordo com o a [documentação](https://kotlinlang.org/docs/object-declarations.html#companion-objects) do Kotlin, as
declarações de objeto ou object declarations criam um Singleton de maneira fácil. Objetos companheiros, para fazer com
que um Object Declaration consiga compartilhar os membros com uma classe.

Constantes em tempo de
compilação ([Compile-Time Constants](https://kotlinlang.org/docs/properties.html#compile-time-constants)) são properties
imutáveis que não mudam o seu estado, como é o caso de tipos primitivos ou Strings, e que não tenham um getter
personalizado.

Todas as classes herdam de `Any`.

## Sintaxe

A utilização do `;` ponto e vírgula ao final de uma linha de código é opcional, porém é um caractere inesperado e
evitado pela comunidade.

Ao atribuir uma variável do tipo primitivo é enviado uma cópia do seu valor e não a sua referência. Em objetos de classe
a referência ao mesmo objeto de memória é que é atribuída.

## Fluxos

Documentação referente à [condições e laços de repetição](https://kotlinlang.org/docs/control-flow.html) do Kotlin.

## Modificadores de acesso

https://kotlinlang.org/docs/visibility-modifiers.html#local-declarations

## Properties

https://kotlinlang.org/docs/properties.html#overriding-properties
Adicionando valor default no construtor padrão torna a propriedade opcional na criação pelo constructor.

## Argumentos nomeados

Enviando parâmetros como [argumentos nomeados](https://kotlinlang.org/docs/functions.html#named-arguments) é possível
enviar em qualquer ordem:
Por exemplo: `val contaAlex = Conta(numero = 1000, titular = "Alex")`.

## Convenção de código em Kotlin

confira a página da documentação que destaca todas
as [convenções](https://kotlinlang.org/docs/reference/coding-conventions.html) esperadas em um código em Kotlin.

## Construtor Secundário

Exemplo:

```kotlin
class Gerente : Funcionario {

    val senha: Int

    constructor(
        nome: String,
        cpf: String,
        salario: Double,
        senha: Int
    ) : super(
        nome = nome,
        cpf = cpf,
        salario = salario
    ) {
        this.senha = senha
    }

    //métodos
}
```

## Modificadores de acesso

Além de public, private ou protected, no Kotlin temos o modificador de acesso conhecido como `internal`, que tem o
objetivo de restringir o acesso para o módulo do projeto. Módulo tem uma melhor discussão quando
utilizamos [build tools](https://medium.com/@alex.felipe/entenda-o-que-s%C3%A3o-build-tools-do-mundo-java-682a26e38ff2)
em nossos projetos. Para mais detalhes, confira
a [documentação](https://kotlinlang.org/docs/tutorials/kotlin-for-py/visibility-modifiers.html).

## Atalhos Itellij

- Ctrl + Alt + L: Identa
- Ctrl + Alt + M: Refactor to function
- Shift + Delete: Exlui linha
- Ctrl + D: Duplica linha
- Shift + F10: Executa código
- Shift + F6: Renomeia
- Ctrl + Alt + M: Refactor Extract Function
- Alt + Enter: Extract from current file, convert function to property, to row string literal
- F5: Duplica classe
- Alt + Insert: Adicionar arquivo ao projeto, override toString, etc
- Ctrl + Alt + M: Extract functions
- Ctrl + N: Busca de arquivos por nome
- Alt + Shift + x: fecha todas os arquivos
- Ctrl + Shift + N: Busca/navega por funções
- Ctrl + Alt + O: Organiza imports
- Ctrl + B: Entra an implementação
- Shift + F9: Debug
- Ctrl + H: Mostra hierarquia da classe

## Polimorfismo

Uma classe abstrata que estende outra classe abstrata não precisa implementar método abstrato. Classes concretas sim. Em
uma interface não é possível manter um estado, armazenar valor. Para implementação de mesma função em elementos que não
herdam do mesmo pai tem-se a possibilidade de utilizar `interfaces`. Uma abordagem melhor visto do que a utilização da
herança. Artigo do blog da
Caelum: [Como não aprender orientação a objetos: Herança](https://blog.caelum.com.br/como-nao-aprender-orientacao-a-objetos-heranca/)
. Caso a função tenha a mesma implementação, esta implementação pode estar na interface. Interfaces são interessantes
para usar o polimorfismo em classes que tem o mesmo comportamento, mas não herdam da mesma classe.

## Sobrecarga

Diferentes implementações para a mesma estrutura.

## Expressions

- If expression:

``` kotlin
val valorComTaxa: Double? = if (valorRecebido != null) {
    valorRecebido + 0.1
} else {
    null
}
```

- When expression:

```kotlin
val valorComTaxa: Double? = when {
    valorRecebido != null -> {
        valorRecebido + 0.1
    }
    else -> {
        null
    }
}
```

- Early return

```kotlin
val valorComTaxa: Double? = valorComTaxa(100.0)

fun valorComTaxa(valorRecebido: Double?): Double? {
    if (valorRecebido != null) {
        return valorRecebido + 0.1
    }
    return null
}
```

## Exceptions

Ao criarmos uma exception precisamos herdar diretamente de Exception, pois é a classe base para identificação de uma
classe como exceção. Por mais que o Kotlin opere na JVM, não
existem [exceções checked](https://kotlinlang.org/docs/exceptions.html#checked-exceptions) no Kotlin.

## Delegation

Além da sintaxe `override`, com apenas a syntax `Autenticavel by titular`, é feita a delegação de implementação do
método Autnetica da interface `Autenticavel` para a classe Cliente. A restrição nesse caso é que precisamos operar com
properties val, pois não é possível modificar a implementação depois de criarmos uma conta.

```kotlin
abstract class Conta(
    val titular: Cliente,
    val numero: Int
) : Autenticavel by titular
```

## Coleções, arrays e listas

- `For` não permite edição direta dos objetos, utilizar índice.
- `ForEach` permite alteração dos valores do próprio arrayListCurso.array.
- `.map` retorna um novo arrayListCurso.array com valores alterados.

- `Ranges` podem ser utilizados para simplificar condições que visam identificar intervalos.
- `.all` retorna boolean que indica se todos os elementos satisfazem determinada condição.
- `.any` retorna boolean que indica se pelo menos um dos elementos satisfazem determinada condição.
- `.filter` retorna novo arrayListCurso.array com valores filtrados.
- `.find` retorna valor do item contido no arrayListCurso.array, dentro da condição.
- `.contain` retorna boolean que indica se valor está contido no arrayListCurso.array.
- [Extension function](https://kotlinlang.org/docs/extensions.html#extension-functions) de arrayListCurso.array:
  ```kotlin
  fun Array<BigDecimal>.somatoria(): BigDecimal {
      return this.reduce { acumulador, valor ->
      acumulador + valor
      }
  }
  ```
  
 - [Reduce](https://kotlinlang.org/docs/collection-aggregate.html#fold-and-reduce) aplica uma operação aos elementos do arrayListCurso.array e retornam o resultado acumulado.
Reduce utiliza no primeiro passo, o primeiro elemento do arrayListCurso.array como parâmetro `acumulador` e o segundo índice como elemento da função. Por exemplo:  
```
val numbers = listOf(5, 2, 10, 4)

val sumDoubledReduce = numbers.reduce { sum, element -> sum + element * 2 }
// Primeiro passo: 5 + 2 * 2; segundo passo: 9 + 10 * 2, terceiro passo: 29 + 4 * 2
println(sumDoubledReduce) // Resultado: 37
```
 - [Fold](https://kotlinlang.org/docs/collection-aggregate.html#fold-and-reduce) aplica uma operação aos elementos do arrayListCurso.array e retornam o resultado acumulado.
Fold recebe um valor inicial e utiliza-o como valor do `acumulador` no primeiro passo. Por exemplo:  
```kotlin
val numbers = listOf(5, 2, 10, 4)

val sumDoubled = numbers.fold(0) { sum, element -> sum + element * 2 }
// Primeiro passo: 0 + 5 * 2; segundo passo: 10 + 2 * 2, terceiro passo: 14 + 10 * 2, quarto passo: 34 + 4 * 2
println(sumDoubled) // Resultado: 42
```

 - A lista não tem tipos primitivos padrão, como arrays "intArrayOf", "intArray" por exemplo. 
   Listas possuem tamanhos dinâmicos.
 - "filterNotNull()", remove da lista objetos nulos
 - Lista é imutável, evita alterações na própria lista, não possuindo o método `sortBy` por exemplo. `SortedBy` do List cria uma nova coleção.
 - As coleções permitem realizar operações aritméticas para devolver novas coleções. Isso é possível a partir da técnica de [operator overloading](https://kotlinlang.org/docs/operator-overloading.html).
   
 - Diagrama de interfaces de coleções do Kotlin:
<img src="https://kotlinlang.org/docs/images/collections-diagram.png">
   
#### Iterable
Iterable sendo a interface mais genérica não possui funções mais específicas, como `size`, por exemplo.

#### List
 - `Lista` é uma coleção ordenada, dá acesso aos seus elementos a partir do índice. É possível que hajam elementos repetidos na lista.

#### Set
 - `Set` é uma coleção de elementos únicos. A ordem não tem muita relevância.
 - [Comportamentos exclusivos do Set](https://kotlinlang.org/docs/set-operations.html):
    - `union()`: devolve um novo Set com a soma dos elementos dos conjuntos
    - `subtract()`: devolve um novo Set subtraindo os elementos contidos no segundo conjunto
    - `intersect()`: devolve um novo Set apenas com os elementos contidos em ambos os conjuntos

#### Map
 - `Map` é uma estrutura de chave e valor na qual a chave não se repete.
 - Os valores podem ser duplicados
 - [Infix](https://kotlinlang.org/docs/functions.html#infix-notation) `to` tem uma performance menor do que `Pair`
 - `MutableMap` é o tipo map que permite edição
 - `putIfAbsent()` adiciona elemento apenas se a chave for inexistente
 - AssociateBy usa o próprio item da lista como valor do map.
 - `GroupingBy()` agrupa elementos de uma Collection e em vez de um Map, retorna uma instância de Grouping.
Tendo acesso a eachCount(), fold(), reduce(), aggregate().
   
##### Filtros específicos de Map
[Documentação](https://kotlinlang.org/docs/map-operations.html).

Recuperação de chave e valor:
 - getValue()
 - getOrElse()
- getOrDefault()
- keys e values

Filtragem:
- filter()
- filterKeys()
- filterValues()

Comportamentos de escrita:
 - Operadores plus (+) e minus(-)
 - putAll()
 - Operadores plusAssign (+=) e minusAssign (-=)
 - keys.remove()
 - values.remove()

## Data class
[Data Classes](https://kotlinlang.org/docs/data-classes.html) com o objetivo principal de manter dados.

## Recursos do paradigma Funcional

Kotlin é uma linguagem é multiparadigma, oferece suporte para Orientaçã a Objeto como também para Progamação Funcional.
Na orientação a objeto quando se executa um código com os mesmos argumentos espera-se retornar o mesmo valor, sem que variáveis externas influenciem o resultado.
O paradigma funcional trata a computação como uma avaliação de funções matemáticas e que evita estados ou dados mutáveis.
Enfatiza a aplicação de funções, em detrimento de mudanças de estado da aplicação.

### Variáveis do tipo função
Uma variável referencia uma função preexistente, podendo ser chamada a sua execução através desa variável.
`::` indica a referência da função e não a sua execução.

```kotlin
    val minhaFuncao: () -> Unit = ::teste

    fun teste() {
        println("Executa teste!")
    }
```

#### Operador Invoke
Para inicializar uma variável do tipo função com um objeto, implementamos uma função na classe.
Ao implementar a função, sobrescrevemos o método `invoke()` e, como foi mencionado, ele é um "operador especial".
Esse operador é conhecido como sobrecarga de operador ou operator overloading.

Isso significa que não precisamos implementar um tipo função para obter um `invoke()` em uma classe, podemos também adicionar como um overloading operator.
Além disso, podemos escrever mais de um operador invoke utilizando override.
```kotlin
class Teste {
    operator fun invoke() {
        println("executa invoke do Teste")
    }
}
```

 - Expressão Lambda e Função Anônima - não é possível reutilizar a implementaçao em outra variável.
 - Expressão Lambda, a última instrução da expressão lambda é o seu retorno.
    O `it` é acessível caso a expressão possua apenas um parâmetro.
 - [Reflection](https://kotlinlang.org/docs/reflection.html)
 - [Return at labels](https://kotlinlang.org/docs/returns.html#return-at-labels)

### High-order functions
Uma função de [ordem superior](https://kotlinlang.org/docs/lambdas.html#higher-order-functions) é uma função que leva funções como parâmetros ou retorna uma função.

### [Scope Functions](https://kotlinlang.org/docs/scope-functions.html)
Bloco de código com objeto de contexto receiver/receptor: apply(), run()...
Ao utilizar o `.run` não é necessário referenciar o nome do objeto.
 - `Let`: Opera com o resultado de uma ou mais funções e com valores nulos.
Cria variável com um escopo mais limitado com alguma modificação.
 - `With`: proporciona acesso direto às propriedades de um objeto, quando não se deseja retornar valor ou se quer retornar o objeto alterado.
    - Não é uma função de extensão.
    - Chama membros do objeto de contexto diretamente
    - Também é usado para computar algo com o receptor e retornar o valor
    - Em código lemos "com esse objeto, faça"
 - `Run`:
    - Similar à proposta do *let*, mas o uso é parecido com o with.
    - Utiliza a inicialização de um objeto.
    - Retorna a computação do objeto
 - `Run sem extensão`
    - Sem objeto de contexto
    - Similar a execução do with, mas não recebe nenhum objeto
    - Executa um bloco de código e retorna sua computação
   
 - `Apply`: receptor
    - Recebe e retorna o objeto de contexto
    - Configura o objeto de contexto sem fazer computações
    - Em código lemos "Aplique as seguintes atribuições ao objeto"
   
 - `Also`: argumento
    - Igual ao *apply* em retorno
    - Realiza ações a mais que não modificam o objeto
    - A remoção do also não deve afetar o programa
    - Em código lemos "Também faça isso"
 - `TakeIf` e `takeUnless`
    - Utilizados para verificar o estado do objeto
    - Retorna o objeto ou null dependendo do predicado:
         - takeIf() - retorna o objeto caso de predicato Verdadeiro, caso contrário retorna null
   
 - Valor de retorno:
    - `apply` e `also` retornam o objeto de contexto, this
    - `let`, `run`, e `with` retornam o lambda resultado. Interessantes para quando não há um retorno, apenas execução de uma instrução.
   
Guia alura de [estudo sobre Scope Functions](https://github.com/alura-cursos/kotlin-funcional/blob/resources/funcoes-de-escopo-kotlin.pdf).

Um outro tópico que não foi visto e curso mas utiliza toda a base apresentada, é o [Type-Safe Builders](https://kotlinlang.org/docs/reference/type-safe-builders.html), uma técnica que permite construir objetos a partir de DSL (Domain-Specific Language).