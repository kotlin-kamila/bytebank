package modelo

import FalhaAutenticacaoException
import SaldoInsuficienteException

abstract class Conta(
    var titular: Cliente,
    val numero: Int
) : Autenticavel {
    var saldo = 0.0
        protected set(valor) {
            if (valor > 0.0) {
                field = valor
            }
        }

    // Object Declaration (Singleton), caso não esteja nomeado o nome atribuído pelo Kotlin é 'Companion'
    companion object Contador {
        var total = 0
            private set
    }

    init {
        total++
    }

    fun deposita(valor: Double) {
        saldo += valor
    }

    abstract fun saca(valor: Double)

    fun transfere(valor: Double, destino: Conta, senha: Int) {
        if (saldo < valor) {
            throw SaldoInsuficienteException(
                mensagem = "O saldo é insuficiente. Saldo atual: $saldo, valor solicitado: $valor"
            )
        }

        if (!autentica(senha)) {
            throw FalhaAutenticacaoException()
        }

        saldo -= valor
        destino.deposita(valor)
    }

    override fun autentica(senha: Int): Boolean {
        return titular.autentica(senha)
    }

}