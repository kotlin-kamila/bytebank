package teste

import SaldoInsuficienteException

fun testaPilha() {

    fun funcao2() {
        println("Início funcao2")

        for (i in 1..5) {
            println(i)
            throw SaldoInsuficienteException()
        }
        println("Fim funcao2")
    }

    fun funcao1() {
        println("Início função1")
        try {
            funcao2()
        } catch (e: SaldoInsuficienteException) {
            e.printStackTrace()
            println("SaldoInsuficienteException capturada!")
        }
        println("Fim funcao1")
    }

}