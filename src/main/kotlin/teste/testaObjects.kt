package teste

import modelo.Autenticavel
import modelo.Conta
import testaAutenticacao
import testaComportamentosConta
import testaContasDiferentes
import testaFuncionarios

fun testaObjects() {
    val valeria = object : Autenticavel {
        val nome: String = "Valeria"
        val cpf: String = "123.123.123-45"
        val senha: Int = 1234

        override fun autentica(senha: Int) = this.senha == senha
    }

    println("Nome do object: ${valeria.nome}")
    testaAutenticacao()
    testaContasDiferentes()
    testaComportamentosConta()
    testaFuncionarios()

    println()
    println("Total de contas: ${Conta.total}")
    println("Total de contas (compartilha membro 'total'): ${Conta.total}")
}