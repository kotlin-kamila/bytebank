import modelo.*

fun testaContasDiferentes() {

    println()
    println("- Testa Contas Diferentes - ")

    val alex = Cliente(
        nome = "Alex",
        cpf = "",
        senha = 1234,
        endereco = Endereco(
            logradouro = "Rua do Vaqueiro",
            cidade = "Fortaleza"
        )
    )
    val gui = Cliente(nome = "Guilherme", cpf = "", senha = 1234)

    val contaCorrente = ContaCorrente(
        titular = alex,
        numero = 1000
    )
    val contaPoupanca = ContaPoupanca(
        titular = Cliente(
            nome = "Maria",
            cpf = "",
            senha = 1234
        ),
        numero = 1001
    )
    val contaSalario = ContaSalario(
        titular = gui,
        numero = 1002
    )

    contaCorrente.deposita(1000.0)
    contaPoupanca.deposita(1000.0)

    println(contaCorrente.titular.nome)
    println(contaCorrente.titular.endereco.cidade)
    println("saldo corrente: ${contaCorrente.saldo}")

    println(contaPoupanca.titular.nome)
    println(contaPoupanca.titular.endereco.cidade)
    println("saldo poupança: ${contaPoupanca.saldo}")

    contaCorrente.saca(100.0)
    contaPoupanca.saca(100.0)
    contaSalario.saca(100.0)

    println("saldo após saque corrente: ${contaCorrente.saldo}")
    println("saldo após saque poupança: ${contaPoupanca.saldo}")
    println("saldo após saque salário: ${contaSalario.saldo}")

    contaCorrente.transfere(100.0, contaPoupanca, 1234)
    println("saldo corrente após tranferir para poupança: ${contaCorrente.saldo}")
    println("saldo poupança após receber transferência: ${contaPoupanca.saldo}")

    contaPoupanca.transfere(200.0, contaCorrente, 1234)
    println("saldo poupança após tranferir para corrente: ${contaPoupanca.saldo}")
    println("saldo corrente após receber transferência: ${contaCorrente.saldo}")

    contaCorrente.transfere(100.0, contaSalario, 1234)

    println("saldo corrente após transferir para salário: ${contaCorrente.saldo}")
    println("saldo salário após receber transferência: ${contaSalario.saldo}")

    contaPoupanca.transfere(200.0, contaSalario, 1234)

    println("saldo poupança após transferir para salário: ${contaPoupanca.saldo}")
    println("saldo salário após receber transferência: ${contaSalario.saldo}")
}
