import modelo.Analista
import modelo.CalculadoraBonificacao
import modelo.Diretor
import modelo.Gerente

fun testaFuncionarios() {

    println()
    println("- Testa Funcionarios - ")

    val leticia = Diretor(
        nome = "Leticia",
        cpf = "123.123.123-00",
        salario = 1000.0,
        plr = 100.0,
        senha = 1234
    )

    var paulo = Gerente(
        nome = "Paulo",
        cpf = "123.456.123-00",
        salario = 2000.0,
        senha = 1234
    )
    var maria = Analista(
        nome = "Maria",
        cpf = "123.456.123-00",
        salario = 2000.0
    )
    println("Nome: ${leticia.nome}")
    println("Bonificação da Leticia: ${leticia.bonificacao}")

    println("Nome: ${paulo.nome}")
    println("Bonificação do Paulo: ${paulo.bonificacao}")

    if (paulo.autentica(1234)) {
        println("Autenticado com sucesso")
    } else {
        println("falha na autenticacao")
    }

    val calculadora = CalculadoraBonificacao()
    calculadora.registra(leticia)
    calculadora.registra(paulo)
    calculadora.registra(maria)

    println("Total de bonificacoes: ${calculadora.total}")
}

