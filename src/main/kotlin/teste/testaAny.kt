package teste

import modelo.Endereco

fun testaAny() {
    val endereco = Endereco(
        logradouro = "Padre Pedro",
        bairro = "Messejana",
        numero = 317,
        cep = "60873-184"
    )
    val enderecoNovo = Endereco(
        logradouro = "Padre Pedro",
        bairro = "Messejana",
        numero = 317,
        cep = "60873-184"
    )

    println("Endereco é igual ao EnderecoNovo? ${endereco.equals(enderecoNovo)}")
    println("Endereco hashcode: ${endereco.hashCode()}")
    println("EnderecoNovo hashcode: ${enderecoNovo.hashCode()}")
    println(endereco.toString())
}
