import modelo.Cliente
import modelo.ContaCorrente

fun testaComportamentosConta() {

    println()
    println("- Testa Comportamentos Conta - ")

    val alex = Cliente(nome = "Alex", cpf = "", senha = 1234)
    val sandro = Cliente(nome = "Sandro", cpf = "", senha = 1234)

    val contaAlex = ContaCorrente(numero = 1000, titular = alex)
    contaAlex.deposita(200.0)

    val contaSandro = ContaCorrente(sandro, 1001)
    contaSandro.deposita(300.0)

    contaAlex.deposita(50.0);
    contaAlex.saca(25.0)
    println("${contaAlex.titular.nome}, saldo: ${contaAlex.saldo}")

    println("${contaSandro.titular.nome}, saldo: ${contaSandro.saldo}")

    try {
        contaSandro.transfere(200.0, contaAlex, 1234)
        println("Transaferência realizada")
    } catch (e: SaldoInsuficienteException) {
        println("Falha ao transferir.")
        e.printStackTrace()
    } catch (e: FalhaAutenticacaoException) {
        println("Falha ao transferir")
        println("Falha na autenticação")
        e.printStackTrace()
    } catch (e: Exception) {
        println("Erro desconhecido")
        e.printStackTrace()
    }

    println()
    println("${contaAlex.titular.nome}, saldo: ${contaAlex.saldo}")
    println("${contaSandro.titular.nome}, saldo: ${contaSandro.saldo}")

}