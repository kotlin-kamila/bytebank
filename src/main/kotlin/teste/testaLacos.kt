fun testaLacos() {
    var i = 0
    while (i < 5) {
        println("Ao infinito e além: $i")
        i++
    }

    for (i in 8 downTo 1 step 2) {

        if (i == 4) {
            continue // Pula o índice 4 e continua o laço
        }
        if (i == 3) {
            break
        }
        val titular: String = "Alex $i"
        val numeroConta: Int = 1000 + i
        var saldo: Double = i + 10.0

        println("Titular: $titular")
        println("Número da conta: $numeroConta")
        println("Saldo da conta: $saldo")

        testaCondicoes(saldo)
    }

    // Utilizando break dentro de um loop aninhado
    loop@ for (i in 1..10) {
        println("conta i: $i")
        for (j in 1..10) {
            println("agência j: $j")
            if (j == 5) break@loop
        }
    }
}
