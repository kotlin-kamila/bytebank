package teste

import modelo.Endereco
import java.lang.IllegalStateException

fun testaNullSafety() {
    var endereco: Endereco? = Endereco(cep = "60873-146")
    val logradouroNovo: String? = endereco?.logradouro

    // Função de escopo Let
    fun castInt(valor: Any) {
        // as? Se não conseguir converter devolve null
        val numero: Int? = valor as? Int
    }

    endereco?.let {
        println("LET 1")
        println(it.logradouro)
        val tamanhoComplemento: Int =
            it.complemento.length ?: throw IllegalStateException("Complemento não pode ser vazio")
        println("Tamanho complemento: $tamanhoComplemento")

        castInt("t")
    }

    endereco?.let { e: Endereco ->
        println("LET 2")
        println(e.logradouro.length)
        println(e.cep)
    }

}