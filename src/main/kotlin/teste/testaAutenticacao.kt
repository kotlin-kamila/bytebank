import modelo.Cliente
import modelo.Diretor
import modelo.Gerente
import modelo.SistemaInterno

fun testaAutenticacao() {

    println()
    println("- Testa Autenticacao - ")

    val gerente = Gerente(
        nome = "Joao",
        cpf = "111.111.11-11",
        salario = 1000.0,
        senha = 1234
    )
    val diretor = Diretor(
        nome = "Fran",
        cpf = "222.222.222-22",
        salario = 1000.0,
        senha = 1234,
        plr = 222.0
    )

    var cliente = Cliente(
        nome = "Faustao",
        cpf = "333.333.333-33",
        senha = 1234
    )

    val sistema = SistemaInterno()
    sistema.entra(gerente, 1234)
    sistema.entra(diretor, 1233)
    sistema.entra(cliente, 1234)
}
