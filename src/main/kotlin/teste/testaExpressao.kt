package teste

fun testaExpressao() {
    val entrada: String = "1,3"

    val valorRecebido: Double? = try {
        entrada.toDouble()
    } catch (e: NumberFormatException) {
        println("Erro NumberFormatException Capturado!")
        e.printStackTrace()
        println("Message: ${e.message}")
        println("StackTrace: ${e.stackTrace}")
        null
    }

    val valorComTaxa: Double? = if (valorRecebido != null) {
        valorRecebido + 0.1
    } else {
        null
    }

    if (valorRecebido != null) {
        println("Valor recebido: $valorRecebido")
    } else {
        println("Valor inválido!")
    }
}