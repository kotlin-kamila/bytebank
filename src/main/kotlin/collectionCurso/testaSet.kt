package collectionCurso

fun testaSet() {
    println("-- Testa Set --")

    val assistiramCursoAndroid: MutableSet<String> = mutableSetOf("Almir", "Fábio", "Higor", "Heitor")
    val assistiramCursoKotlin: MutableSet<String> = mutableSetOf("Almir", "Israel", "Heitor")

    val assistiramAmbos = mutableSetOf<String>()
    assistiramAmbos.addAll(assistiramCursoAndroid)
    assistiramAmbos.addAll(assistiramCursoKotlin)
    // Não adiciona pois já existe elemento com esse valor no SetList
    assistiramAmbos.add("Israel")

    println(assistiramAmbos)

    println("--")
    println(assistiramCursoAndroid + assistiramCursoKotlin)

    println("Utilizando função `union` através de infix:")
    println(assistiramCursoAndroid union assistiramCursoKotlin)

    println("Substract:")
    println(assistiramCursoAndroid - assistiramCursoKotlin)
    println(assistiramCursoAndroid subtract assistiramCursoKotlin)

    println("Interseção entre os conjuntos:")
    println(assistiramCursoAndroid intersect assistiramCursoKotlin)

    val assistiramList = assistiramAmbos.toMutableList()
    assistiramList.add("Almir")
    println("List permitindo conteúdo repetido na lista:")
    println(assistiramList)

    println("Removendo itens repetidos apenas convertendo para Set:")
    println(assistiramList.toSet())
}