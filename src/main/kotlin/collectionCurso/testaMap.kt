package collectionCurso

fun testaMap() {

    println("-- Testa Map --")
    // Map (key : numero do pedido, value: valor do pedido)
    val pedidos: MutableMap<Int, Double> = mutableMapOf(
        Pair(1, 20.0),
        Pair(2, 34.0),
        3 to 50.0
    )

    println(pedidos)

    // Leitura
    val pedido = pedidos[5]
    pedido?.let {
        println("Busca valor do Pedido pela key: $it")
    }
    for (p: Map.Entry<Int, Double> in pedidos) {
        println("Número do pedido: ${p.key}")
        println("Valor do pedido: ${p.value}")
    }

    // Inserção
    pedidos[4] = 70.0
    pedidos.put(5, 80.0)
    println("Adiciona valores:")
    println(pedidos)

    pedidos[1] = 100.0
    println("Altera valor:")
    println(pedidos)

    // Só adciona se a chame for inexistente
    pedidos.putIfAbsent(6, 120.0)
    pedidos.putIfAbsent(6, 140.0)
    println(pedidos)

    println("Remove se o valor for igual ao segundo parâmetro:")
    pedidos.remove(6, 120.0)
    println(pedidos)
}