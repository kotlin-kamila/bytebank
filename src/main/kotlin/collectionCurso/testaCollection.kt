package collectionCurso


fun testaCopia() {
    println("-- Testa Cópia --")

    val banco = BancoDeNomes()
    banco.adicionaDado("Kamila")

    // Copia de banco.nomes apontando pra outra referência de memória
    val nomesSalvos: Collection<String> = banco.nomes
    banco.adicionaDado("Alex")

    println("""Valor de "banco.nomes":""")
    println(banco.nomes)
    println("Valor de objeto cópia:")
    println(nomesSalvos)
}

class BancoDeNomes {

    val nomes: Collection<String> get() = Companion.dados.toList()

    fun adicionaDado(nome: String) {
        Companion.dados.add(nome)
    }

    // Ao utilizar o Companion temos o mesmo valor singleton para "dados" para qualquer instância de BancoDeNomes
    companion object {
        private val dados = mutableListOf<String>()
    }
}

fun testaColecao() {

    println("-- Testa Coleção --")

    val nomes: MutableCollection<String> = mutableListOf(
        "Kaiara", "Darcy", "Janaina", "Carol", "Almir", "Cristiano"
    )

    nomes.add("Paulo")
    nomes.remove("Cristiano")

    for (nome in nomes) {
        println(nome)
    }

    println("Tem o nome 'Almir'? ${nomes.contains("Almir")}")
    println("Número de nomes: ${nomes.size}")
    println("Fim Main!")
}
