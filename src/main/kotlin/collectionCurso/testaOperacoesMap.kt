package collectionCurso

fun testaOperacoesMap() {

    println("-- Testa operações Map --")

    val pedidos: MutableMap<Int, Double?> = mutableMapOf(
        Pair(1, 20.0),
        Pair(2, 34.0),
        3 to 50.0,
        4 to null,
        5 to 150.0,
        6 to 70.0,
        7 to 75.0
    )

    println("* OPERAÇÕES DE LEITURA E FILTRO *")

    println("-- Pedidos KEYS --")
    println(pedidos.keys)
    for (numero in pedidos.keys) {
        println("Pedido key: $numero")
    }

    println("-- Pedidos Values --")
    println(pedidos.values)
    for (valor in pedidos.values) {
        println("Pedido value: $valor")
    }
    println()

    println("-- Filter --")
    val pedidosFiltrados = pedidos.filter { (key, value) ->
        key % 2 == 0
    }
    val pedidosImpares = pedidos.filterKeys { key ->
        key % 2 != 0
    }
    val pedidosMaiores = pedidos.filterValues { value ->
        value != null && value > 70
    }
    println("Pedidos filtrados: $pedidosFiltrados")
    println("Pedidos ímpares: $pedidosImpares")
    println("Pedidos maiores: $pedidosMaiores")
    println()

    println("-- .getOrElse() --")
    val numeroPedido = 5;
    val valor: Double? = pedidos.getOrElse(numeroPedido, {
        0.0
    })
    println("Valor da chave $numeroPedido é $valor, caso não exista recebe 0.0")

    println("-- .getOrDefault() --")
    val orDefault = pedidos.getOrDefault(6, -1.0)
    println("Valor da key inexistente: $orDefault")

    println("-- .get() --")
    val valorPedido4 = pedidos.get(4)
    val valorPedido5 = pedidos.get(5)

    println("Uma chave existente no map com valor null tem o memso valor capturado de uma chave não existente no map (null):")
    println(valorPedido4)
    println(valorPedido5)

    println("-- .getValue() --")
    val valorPedidoGetValue4 = pedidos.getValue(4)
    println("Valor de chave existente nulla com .getValue:")
    println(valorPedidoGetValue4)
    println()

//    println("Uma chave não existente no map provoca exception com .getValue:")
//    val valorPedidoGetValue5 = pedidos.getValue(50)
//    println(valorPedidoGetValue5)

    println("* SOMA E SUBTRAÇÃO DE VALORES *")
    println("-- Plus Operator(+): Criando novo map --")
    println(pedidos + Pair(8, 90.0))
    println("Objeto map 'pedidos' inicial é mantido: $pedidos")

    println(pedidos + mapOf(7 to 90.0, 8 to 20.0))
    println("Objeto map 'pedidos' inicial é mantido: $pedidos")

    println(pedidos + (7 to 143.0))
    println("Objeto map 'pedidos' inicial é mantido: $pedidos")

    println("-- Minus Operator(-): retira itens pela key --")
    println(pedidos - 6)
    println(pedidos - listOf(6, 7))
    println("Objeto map 'pedidos' inicial é mantido: $pedidos")
    println()

    println("* OPERAÇÕES DE ESCRITA *")

    println("-- Inserção --")
    pedidos.putAll(listOf(8 to 90.0, 9 to 20.0))
    println(pedidos)

    pedidos += listOf(10 to 65.0, 11 to 65.0)
    println(pedidos)

    println("-- Remoção --")
    pedidos.remove(4)
    println(pedidos)

    pedidos.keys.remove(1)
    println(pedidos)

    pedidos.values.remove(65.0)
    println("Remove o primeiro que encontra com esse valor (65.0):")
    println(pedidos)

    println("Removendo com '-=':")
    pedidos -= 6
    println(pedidos)

}