package collectionCurso

fun testaCollectionTransformation() {
    println("-- Testa Collections Transformations --")

    println("-- Mapping --")
    val turmas = setOf("A", "B", "C", "D")
    println(turmas.map { "Turma $it" })

    val pedidos = listOf(
        Pedido(1, 20.0),
        Pedido(2, 60.0),
        Pedido(3, 30.0),
        Pedido(4, 70.0)
    )
    println(pedidos)
    val pedidosMapeados: Map<Int, Pedido> = pedidos.associate { pedido: Pedido ->
        Pair(pedido.numero, pedido)
    }
    println(pedidosMapeados)

    println("-- AssociateBy --")
    println("AssociateBy usa o próprio item da lista como valor do map.")
    val pedidosMapeadosAssociateBy: Map<Int, Pedido> = pedidos.associateBy { pedido ->
        pedido.numero
    }
    println(pedidosMapeadosAssociateBy)
    println("Buscando pedido pelo atributo número")
    println(pedidosMapeados[3])

    println("-- AssociateWith --")
    val pedidosFreteGratis: Map<Pedido, Boolean> = pedidos.associateWith { pedido ->
        pedido.valor > 50.0
    }
    println(pedidosFreteGratis)
    println("Buscando boolean por pedido no map:")
    println("Pedido 1 possui frete grátis?")
    println(pedidosFreteGratis[Pedido(1, 20.0)])

//    val mapaFreteGratis: Map<Boolean, Pedido> = pedidos.associateBy { pedido -> pedido.valor > 50.0 }
//    println("Um único true e um único false pois a chave é única em um Map, por isso não é interessante")
//    println(mapaFreteGratis)

    println("-- GroupBy --")
    println("- Pedidos agrupados:")
    val pedidosFreteGratisAgrupados: Map<Boolean, List<Pedido>> =
        pedidos.groupBy { pedido: Pedido ->
            pedido.valor > 50.0
        }
    println(pedidosFreteGratisAgrupados)
    println("- Pedidos com frete grátis: $pedidosFreteGratisAgrupados[true]")

    val nomes = listOf(
        "Ester", "Eugenia", "Enide", "Guilherme", "Marcia", "Marilia", "Murilo", "Pedro", "Telma"
    )
    val agenda: Map<Char, List<String>> = nomes.groupBy { nome ->
        nome.first() // valor será inserido na key
    }
    println("- Agenda: ")
    println(agenda)
    println("Nomes que iniciam com 'E':")
    println(agenda['E'])

    println("-- GroupingBy --")
    val pedidosAgrupados: Grouping<Pedido, Boolean> = pedidos.groupingBy { pedido ->
        pedido.valor > 50.0
    }
    println(pedidosAgrupados.eachCount())
}