package arrayListCurso.list

class Livro(
    val titulo: String,
    val autor: String,
    val anoPublicacao: Long,
    val editora: String? = null
) : Comparable<Livro> {

    override fun compareTo(other: Livro): Int {
        // Tipo long já possui um compareTo method
        return this.anoPublicacao.compareTo(other.anoPublicacao)
    }

}