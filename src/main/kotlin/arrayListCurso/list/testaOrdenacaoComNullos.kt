package arrayListCurso.list

fun testaOrdenacaoComNullos() {

    println("-- Ordenacao com nullos--")

    println("Lista com nulls:")
    listaLivrosComNullos.imprimeComMarcadores()

    println()
    println("Lista de livros por editora:")
    listaDeLivros
        .groupBy { it.editora ?: "Editora desconhecida" } //retorna Map<key , List<Livros>>
        .forEach { (editora: String?, livros: List<Livro>) ->
            println("$editora: ${livros.joinToString { it.titulo }}")
        }
}