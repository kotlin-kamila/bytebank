package arrayListCurso.list

fun testaOrdenacao() {

    println("-- Testa Ordenação --")

    val livro1 = Livro(
        titulo = "Grande Sertão: Veredas",
        autor = "João Guimarães Rosa",
        anoPublicacao = 1956
    )
    val livro2 = Livro(
        titulo = "Minha vida de menina",
        autor = "Helena Morley",
        anoPublicacao = 1942,
        editora = "Editora A"
    )
    val livro3 = Livro(
        titulo = "Memórias Póstumas de Brás Cubas",
        autor = "Machado de Assis",
        anoPublicacao = 1881
    )
    val livro4 = Livro(
        titulo = "Iracema",
        autor = "José de Alencar",
        anoPublicacao = 1865,
        editora = "Editora B"
    )

    val livros: MutableList<Livro> = mutableListOf(livro1, livro2, livro3, livro4)
    livros.add(
        Livro(
            titulo = "Sagarana",
            autor = "João Guimarães Rosa",
            anoPublicacao = 1946
        )
    )
    println("Quantidade de livros: ${livros.size}")
    livros.forEach { println(it.titulo) }
    livros.remove(livro2)

    println()
    println("Quantidade de livros: ${livros.size}")
    livros.forEach { println(it.titulo) }

    val ordenadoPorAnoPublicacao: List<Livro> = livros.sorted()
    println()
    println("Ordenado por ano de publicação:")
    ordenadoPorAnoPublicacao.imprimeComMarcadores()

    val ordenadoPorTitulo = livros.sortedBy { it.titulo }
    println()
    println("Ordenado por titulo:")
    ordenadoPorTitulo.imprimeComMarcadores()

}