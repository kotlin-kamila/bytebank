package arrayListCurso.list

fun testaFiltro() {

    println("-- Testa Filtro--")
    println("Filtrando por autor:")
    val titulosList = listaDeLivros
        .filter { it.autor.startsWith("J") }
        .sortedByDescending { it.anoPublicacao }
        .map { "${it.titulo} - ${it.anoPublicacao}" }
    println(titulosList)
}