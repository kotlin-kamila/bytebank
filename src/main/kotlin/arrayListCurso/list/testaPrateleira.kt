package arrayListCurso.list

fun testaPrateleira() {

    println("-- Testa prateleira --")

    val prateleira = Prateleira(
        genero = "Literatura",
        livros = listaDeLivros
    )

    val porAutor = prateleira.organizarPorAutor()
    val porAnoPublicacao = prateleira.organizarPorAnoPublicacao()

    println("Lista ordenada por autor:")
    porAutor.imprimeComMarcadores()

    println("Lista ordenada por ano de publicação:")
    porAnoPublicacao.imprimeComMarcadores()
}