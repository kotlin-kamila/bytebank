package arrayListCurso.array

fun testaRanges() {

    println()
    println("-- Testa Ranges --")

    val serie: IntRange = 1..10
    for (i in serie) {
        print("$i ")
    }

    for (i in 1..100) {
        if(i % 2 == 0) {
            print("$i ")
        }
    }

    println()
    println("Números pares de 0 até 100:")
    val numerosPares1 = 0..100 step 2
    for (n in numerosPares1) {
        print("$n ")
    }

    println()
    val numerosPares2: IntProgression = 2..100 step 2
    numerosPares2.forEach { print("$it ") }

    println()
    println("DownTo:")
    val numerosParesReverso: IntProgression = 100 downTo 2 step 2
    numerosParesReverso.forEach { print("$it ") }

    val salarios: DoubleArray = doubleArrayOf(1500.55, 9100.0, 2990.99, 8500.0, 10000.0)

    val base = 1500.0
    val topo = 4000.0
    var contador = 0
    for (salario in salarios) {
        if(salario in base..topo) {
            contador++
        }
    }
    println()
    println("Existem $contador salários entre $base e $topo.")

    val alfabeto: CharRange = 'a'..'z'
    val letra = 'k'
    println("Alfabeto: ")
    alfabeto.forEach { print("$it ") }

    println()
    if ('k' in alfabeto) {
        println("O caractere $letra está contido em $alfabeto")
    } else {
        println("O caractere $letra não está no alfabeto!")
    }
}