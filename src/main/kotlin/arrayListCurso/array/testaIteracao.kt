package arrayListCurso.array;

fun testaIteracao() {

    println()
    println("-- Testa Iteração --")

//  For
    val salarios = doubleArrayOf(1500.5, 2300.0, 500.0, 8000.0, 10000.0)

    val aumento = 1.1
    var indice = 0

    for (salario in salarios) {
        // Dentro do for a variável "salario" é imutável
        salarios[indice] = salario * aumento
        indice++
    }

    for (i in salarios.indices) {
        salarios[i] = salarios[i] * aumento
    }
    println("For: ${salarios.contentToString()}")

//  ForEachIndexed
    salarios.forEachIndexed { i, salario ->
        salarios[i] = salario * aumento
    }
    println("ForEachIndexed: ${salarios.contentToString()}")

//  ForEach
    val idadesIntArrayOf: IntArray = intArrayOf(25, 19, 33, 20, 55, 40)
    var menorIdade = Int.MAX_VALUE

    idadesIntArrayOf.forEach { idade ->
        if (idade < menorIdade) {
            menorIdade = idade
        }
    }
    println("ForEach: menor idade = $menorIdade")
}
