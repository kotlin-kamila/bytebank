package arrayListCurso.array

import java.math.BigDecimal

/**
 * Como BigDecimal não é do tipo primitivo precisamos criar a funcao arrayOf
 * String To BigDecimal recomendado para evitar arredondamentos to tipo ponto flutuante (double, float)
 */
fun bigDecimalArrayOf(vararg valores: String): Array<BigDecimal> {
    return Array<BigDecimal>(valores.size) { i ->
        valores[i].toBigDecimal()
    }
}

// “Extension function” de arrayListCurso.array
fun Array<BigDecimal>.somatoria(): BigDecimal {
    return this.reduce { acumulador, valor ->
        acumulador + valor
    }
}

fun Array<BigDecimal>.media(): BigDecimal {
    return if(this.isEmpty()) {
        BigDecimal.ZERO
    } else {
        this.somatoria() / this.size.toBigDecimal()
    }
}