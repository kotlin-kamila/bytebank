package arrayListCurso.array

fun testaAgregacoes() {

    println()
    println("-- Testa Agregações --")

    val idades: IntArray = intArrayOf(10, 12, 33, 19, 40, 67)
    val maiorIdade = idades.maxOrNull()
    val menorIdade = idades.minOrNull()
    val mediaIdades: Double = idades.average()

    println("Maior idade: $maiorIdade")
    println("Menor idade: $menorIdade")
    println("Média: $mediaIdades")

    val todosMaiores: Boolean = idades.all { it >= 18 }
    println("São todos maiores? $todosMaiores")

    val existeMaior: Boolean = idades.any { it >= 18 }
    println("Existe algum maior? $existeMaior")

    val maiores = idades.filter { it >= 18 }
    println("Quais são maiores de idade? $maiores")

    val primeiroMaior = idades.find { it >= 18 }
    println("Primeiro valor que satifaz a condição maior de idade: $primeiroMaior")
}