package arrayListCurso.array

import java.math.BigDecimal
import java.math.RoundingMode

fun calculaSalarios() {

    println()
    println("-- Calcula Salários --")

    // Criando arrayListCurso.array de 5 BigDecimals inicializados com valor Zero
    val salariosTeste = Array<BigDecimal>(5) { BigDecimal.ZERO }

    // Melhorando forma de criar Array BigDecimal
    val salarios = bigDecimalArrayOf("1500.55", "2000.00", "5000.00", "10000.00")
    println("Salários iniciais: ${salarios.contentToString()}")

    // Regra de negócio aumento de salário de 10%, sendo no mínimo 500$ para qualquer valor de salário
    val porcentagemAumento = "1.1".toBigDecimal()
    val salariosComAumento: Array<BigDecimal> = salarios
        .map { salario -> calculaAumentoRelativo(salario, porcentagemAumento) }
        .toTypedArray()

    println("Salários com aumento: ${salariosComAumento.contentToString()}")

    val investimentoInicial = salariosComAumento.somatoria()
    println("Investimento inicial em salários por 1 mês: $investimentoInicial $")

    val qtdMeses = 6.toBigDecimal()
    val investimentoTotal = salariosComAumento.fold(investimentoInicial) { acumulador, salario ->
        acumulador + (salario * qtdMeses).setScale(2, RoundingMode.UP)
    }
    println("O investimento em aumento salarial de $qtdMeses meses é $investimentoTotal $.")

    val mediaTresMaiores = salariosComAumento
        .sorted()
        .takeLast(3)
        .toTypedArray()
        .media()

    val mediaTresMenores = salariosComAumento
        .sorted()
        .takeLast(3)
        .toTypedArray()
        .media()
    println("A média aritmética dos três maiores salários é $mediaTresMaiores")
    println("A média aritmética dos três menores salários é $mediaTresMenores")


}

private fun calculaAumentoRelativo(
    salario: BigDecimal,
    porcentagemAumento: BigDecimal
): BigDecimal {
    return if (salario < "5000".toBigDecimal()) {
        salario + "500".toBigDecimal()
    } else {
        (salario * porcentagemAumento).setScale(2, RoundingMode.UP)
    }
}