package paradigmaFuncional

import modelo.Endereco

fun testaWith() {
    with(Endereco()) {
        logradouro = "Rua Dona Leopoldina"
        numero = 1551
        bairro = "Centro"
        cidade = "Fortaleza"
        estado = "CE"
        cep = "60873-245"
        complemento = "Prédio"

        completo()
    }.let { enderecoCompleto: String ->
        println(enderecoCompleto)
    }
}