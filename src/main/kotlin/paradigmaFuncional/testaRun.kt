package paradigmaFuncional

import modelo.Cliente
import modelo.ContaPoupanca

fun testaRun() {
    val taxaAnual = 0.05
    val taxaMensal = taxaAnual / 12
    println("""Taxa mensal: $taxaMensal""")

    val contaPoupanca = ContaPoupanca(Cliente(nome = "Luiza", cpf = "111.111.111-11", senha = 1234), numero = 1828)

    contaPoupanca.run {
        deposita(1000.0)
        saldo * taxaMensal
    }.let { rendimentoMensal ->
        println("Valor com rendimento mensal: $rendimentoMensal")
    }

    val rendimentoAnual = run {
        var saldo = contaPoupanca.saldo
        repeat(12) { indice ->
            saldo += saldo * taxaMensal
        }
        saldo
    }
    println("Valor com rendimento anual: $rendimentoAnual")
}