package paradigmaFuncional

import modelo.Autenticavel
import modelo.SistemaInterno

fun testaHof() {

    println("-- Testa High Order Function --")

    soma(1, 5, resultado = { valor ->
        println("Imprimindo valor: $valor")
    })
    somaReceiver(1, 5, resultado = {
        println("Imprimindo valor receiver: $this")
    })

    soma(1, 5) {
        println(it)
    }

    val autenticavel = object : Autenticavel {
        val senha = 1234
        override fun autentica(senha: Int) = this.senha == senha
    }

    // HOF - High order function
    val sistema = SistemaInterno()
    sistema.entra(autenticavel, 1234, autenticado = {
        println("Autenticado com sucesso!!")
    })
    sistema.entraReceiver(autenticavel, 1234, autenticado = {
        // Acessando escopo do Sistema Interno
        pagamento()
    })
    SistemaInterno().entra(autenticavel, 1234) {
        println("Mesmo resultado. Autenticado com sucesso!!")
    }
}
// Utilizando implementação como argumento
fun soma(a: Int, b: Int, resultado: (Int) -> Unit) {
    println("- Soma sendo efetuada.")
    resultado(a + b)
    println("depois de Soma")
}
// Chamada do resultado como sendo uma função de extensão do Int
fun somaReceiver(a: Int, b: Int, resultado: Int.() -> Unit) {
    println("- SomaReceiver sendo efetuada.")
    val total = a + b
    total.resultado()
    println("depois de SomaReceiver")
}