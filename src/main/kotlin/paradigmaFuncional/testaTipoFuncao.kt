package paradigmaFuncional

//    testaTipoFuncaoClasse()
//    testaTipoFuncaoReferencia()

fun testaFuncaoAnonima() {
    println("-- Funções anônimas --")
    val minhaFuncaoAnonima = fun(a: Int, b: Int): Int {
        println("Executa função Anônima.")
        return a + b
    }
    println(minhaFuncaoAnonima(20, 30))

    val calculaBonificacaoAnonima: (salario: Double) -> Double = fun(salario): Double {
        println("Calcula bonificação anônima.")
        if (salario > 700.0) {
            return salario + 50
        }
        return salario + 100.0
    }
    println(calculaBonificacaoAnonima(1000.0))
}

fun testaFuncaoLambda() {
    println("-- Expressões Lambda --")
    val minhaFuncaoLambda: (Int, Int) -> Int = { a, b ->
        println("Executa como lambda.")
        a + b
    }
    println(minhaFuncaoLambda(15, 10))

    val calculaBonificacao: (salario: Double) -> Double =
        lambda@{ salario ->
            println("Calcula bonificação.")
            if (salario > 700.0) {
                return@lambda salario + 50
            }
            salario + 100.0
        }
    println(calculaBonificacao(1000.0))
}

fun testaTipoFuncaoClasse() {
    val minhaFuncaoClasse: (Int, Int) -> Int = Soma()
    println(minhaFuncaoClasse(9, 24))
}

fun testaTipoFuncaoReferencia() {
    val minhaFuncao: (Int, Int) -> Int = ::soma
    println(minhaFuncao(47, 25))
}

fun soma(a: Int, b: Int): Int {
    println("Executa função soma!")
    return a + b
}

class Soma : (Int, Int) -> Int {
    override fun invoke(a: Int, b: Int): Int {
        println("Executa invoke da Class Soma.")
        return a + b
    }
}