package paradigmaFuncional

import modelo.Endereco

fun testaFuncoesEscopo() {
    println("-- Testa Funçoes Escopo --")

    run {
        println("Run: execução do run sem extensão")
    }

    val endereco = Endereco()
        .also { endereco ->
            endereco.bairro = "Messejana"
        }
        .apply {
            logradouro = "Rua Pedro de Souza"
            numero = 317
        }

    with(endereco) {
        "With: $logradouro, $numero, $bairro".toUpperCase()
    }.let { e: String ->
        println(e)
    }

    val enderecoPedestre = Endereco(logradouro = "Rua Pedestre Três", numero = 420)
        .apply {
            "Apply: $logradouro, $numero".toUpperCase()
        }.also {
            println("Apply retorna um Endereco e não a String.")
            println("Also: ${it.logradouro}, ${it.numero}".toUpperCase())
        }

//    val enderecoEmMaiusculo = "${endereco.logradouro}, ${endereco.numero}".toUpperCase()
//    println(enderecoEmMaiusculo)

    Endereco(logradouro = "Rua Pedro de Souza", numero = 317)
        .let { endereco ->
            "Let: ${endereco.logradouro}, ${endereco.numero}".toUpperCase()
        }.let(::println)

    val enderecosComComplemento = listOf(
        Endereco(complemento = "casa"),
        Endereco(complemento = "apartamento"),
        Endereco(complemento = "sítio")
    ).filter(predicate = { endereco -> endereco.complemento.isNotEmpty() })
        .let(block = (::println))
}